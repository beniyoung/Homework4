Dijkstra's Algorithm to solve the single source shortest paths problem.
This is implemented in C++ and uses a priority queue.

The program takes in 4 commandline arguements. 

USAGE: (on a linux system)
./FindShortestRoadPath inputFile.gr sourceID targetID outputFileName

program outputs to "outputFileName" The total cost and shortest path
from source to target.

Beni Young
byoung2@uno.edu