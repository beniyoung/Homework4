// Beni Young
// byoung2@uno.edu
// Homework 4: Dijkstra's Algorithm
// Dec 1, 2017
// CSCI 2125

#include <iostream>
#include <queue>
#include <vector>
#include <climits>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;

#define INFINITY INT_MAX // set Infinity to use for initial distance value

const int SIZE = 733846; // Maximum possible number of Arcs. I was thinking about grabbing this from .gr file, but hardcoded for simplicity (and time restraint)
vector<pair<unsigned int, unsigned int> > adj[SIZE]; // Adjacency list
int dist[SIZE]; // Stores shortest distance
int path[SIZE]; // Stores previous vertex in which it has the shortest distance
bool known[SIZE]={0}; //Determines whether the node has been known or unknown

//Custom Comparator for determining priority for priority queue with shortest edge coming first
class Compare
{
 public: 
  bool operator ()(pair<int, int> &vertex1, pair<int, int> &vertex2)
  {
    return vertex1.second > vertex2.second;
  }
};

// Finds shortest distance between sourceID vertex to targetID vertex through Dijkstra's Algorithm using priority queue
void Dijkstra(int sourceID, int targetID)
{
  int v,w; // Adjacent vertex and weight of current vertex

  // make sure the array boolean values of known are all set to false
  for(int j=0; j < SIZE; j++)
    known[j] = 0;
		
  // set initial distances to Infinity and paths to negative values
  for(int i=0; i < SIZE; i++)
    {
      dist[i] = INFINITY;
      path[i] = -1;
    }
	
    priority_queue<pair<int,int>, vector<pair<int,int> >, Compare> pq; // priority queue to store vertex,weight pairs
    
    pq.push(make_pair(sourceID, dist[sourceID]=0)); // pushing the source with distance from itself as 0
    path[sourceID] = sourceID; // set the source path as itself
    
    while(!pq.empty())
    {
        pair<int, int> current = pq.top(); // current vertex. The shortest distance for this has been found
        pq.pop();
        
        
        int cv = current.first, cw = current.second; // cw "current weight" the final shortest distance for this vertex
        if (cv == targetID) // if target is found then exit out
	  break;					
        
        if(known[cv]) //If the vertex is already visited, then there isn't a need to find adjacent vertices
	  continue;
        	
        
        
        for(int i=0; i < adj[cv].size(); i++) // search through all adjacent vertices of current vertex
	  {		
	    v = adj[cv][i].first;  // adjacent vertex
	    w = adj[cv][i].second; // adjacent weight
	
	    if(dist[v] > dist[cv] + w)          // if distance of adjacent vertex is greater then current distance and adjacent weight
	      {                                 // then we update to shortest disance and push the vertex back into queue
		dist[v] = dist[cv]+w;           // also updating path with new previous vertex that gives shortest path
		pq.push(make_pair(v,dist[v])); 
		path[v] = cv;		
	      } 
	  } 
		
	known[cv]=1; // current vertex becomes known		
    } 
} 

int main(int argc, char* argv[]) 
{
  string testline;  // read in file line
  int x, y, weight; // connecting vertex x and  vertex y and the weight between them 
  char a;           // takes in first 'a' character from each line in file and disregards
	
  ifstream myReadFile;
  //myReadFile.open("USA-road-d.NY.gr");
  myReadFile.open(argv[1]); // opens the .gr file taken from commandline
	
  if(!myReadFile) // simple error handling if file is not found.
    {
      cout << "There was a problem reading " << argv[1] << " file.";
      return 0;
    }
           
  while(getline( myReadFile, testline)) 
    {
      // building Graph from .gr file
      if(testline != "" && testline.at(0) == 'a')     // doesn't read in a line unless it starts with a, which contains the vertices and weights
	{
	  stringstream buffer(testline);              // used to convert information on each line to corresponding data type
			
	  buffer >> a >> x >> y >> weight;            // takes in char a and disregards, vertex x, vertex y, weight of edge 
        	   		
	  adj[x].push_back(make_pair(y, weight));     // pushing pair value into adjacent list   		
      	}
    }

  // one way of taking in commandline arguement of sourceID and converting to int
  istringstream ss(argv[2]);
  int sourceID;
  ss >> sourceID;

  // one way of taking in commandline argument of targetID and converting to int
  istringstream tt(argv[3]);
  int targetID;
  tt >> targetID;

  
  vector<int> tmpath;          // temp int vector to store shortest paths
  tmpath.push_back(targetID);  // puts target path in first
  
  Dijkstra(sourceID, targetID); // find shortest path from source to target using Dijkstra's Algorithm

  // write to file using commandline argument as name
  ofstream myfile (argv[4]);
  if (myfile.is_open())
    {    
      myfile<<"Shortest Path from " << sourceID << " to " << targetID <<endl;
      myfile<<"Weight: " << dist[targetID] << endl;
      myfile<<"Path:" << endl;
    }

  // This is all so I can print out the paths from source to target in the order shown in example
  // if i print out using just path[i] then it prints out from target to source
  // for example: using 1 to 1276 using just path[i]
  // 1276
  // 1356
  // 1357
  // 1358
  // 1363
  // 1
  for(int i=0; i < SIZE; i++)
  {
    if(dist[i] != INFINITY) 
      {   		
	if (i == tmpath.back())
   	tmpath.push_back(path[i]);
      }
   		
  }

  // By storing the path values in a temporary vector I can write out the values in reverse order
  // from pushing out the last value until its empty
  if (myfile.is_open())
  {
    while(!tmpath.empty())
    {
      myfile << tmpath.back() << endl;
      tmpath.pop_back();
    }    
    myfile.close();
  }
  else cout << "Unable to open file";

  return 0;
}
