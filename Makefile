# simple makefile for Finding Shortest Path
CC=g++
BIN=FindShortestRoadPath

FindShortestRoadPath: main.o
	$(CC) -o $(BIN) main.o 

.PHONY: clean remove

clean:
	rm -f *.o *~

remove:
	rm -f *.o *~ *.txt $(BIN)
